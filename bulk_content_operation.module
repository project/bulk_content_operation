<?php

use Drupal\bulk_content_operation\BulkContentOperationData;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Implements hook_entity_operation().
 * Add content export import operations to the default operation list of
 * content types.
 *
 * @param $operations
 * An array that contains operation list for entity type.
 */
function bulk_content_operation_entity_operation_alter(array &$operations, EntityInterface $entity) {
  $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $config = \Drupal::config( 'bulk_content_operation.settings' );
  $allowed_types = $config->get( 'allowed_types', [] );
  if (!(\Drupal::currentUser()
    ->hasPermission( 'administer bulk content operation' ))) {
    return;
  }

  $entityTypeId = $entity->getEntityTypeId();
  if ($entityTypeId !== 'node_type') {
    return;
  }

  if (!in_array( $entity->id(), $allowed_types )) {
    return;
  }

  $user = \Drupal::currentUser();
  if ($user->hasPermission( 'administer bulk content operation' )) {
    $operations['export'] = [
      'title' => t( 'Export Content' ),
      'weight' => 99,
      'url' => Url::fromRoute( 'bulk_content_operation.export', [
        'type' => $entity->getOriginalId(),
        'entity' => BulkContentOperationData::NODE_ENTITY,
        'langcode' => $lang,
      ] ),
    ];
    $operations['import'] = [
      'title' => t( 'Import Content' ),
      'weight' => 99,
      'url' => Url::fromRoute( 'bulk_content_operation.import', [
        'type' => $entity->getOriginalId(),
        'entity' => BulkContentOperationData::NODE_ENTITY,
        'langcode' => $lang,
      ] ),
    ];
    $operations['download_template'] = [
      'title' => t( 'Download Template' ),
      'weight' => 99,
      'url' => Url::fromRoute( 'bulk_content_operation.download_template', [
        'type' => $entity->getOriginalId(),
        'entity' => BulkContentOperationData::NODE_ENTITY,
        'langcode' => $lang,
      ] ),
    ];
  }
}
