<?php
/**
 * contains Drupal\bulk_content_operation\BulkContentOperationController
 */

namespace Drupal\bulk_content_operation\Controller;

use Drupal\bulk_content_operation\PHPExcelOperations;
use Drupal\Core\Controller\ControllerBase;

class BulkContentOperationController extends ControllerBase {
  public function export() {
    $message = PHPExcelOperations::export();

    return [
      '#markup' => t( $message ),
    ];
  }

  /**
   * Download Template
   */
  public function downloadTemplate() {
    $message = PHPExcelOperations::downloadTemplate();

    return [
      '#markup' => t( $message ),
    ];
  }
}
